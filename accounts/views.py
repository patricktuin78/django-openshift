from django.views.generic import TemplateView


class SettingsView(TemplateView):
    template_name = 'accounts/account_settings.html'

    def get_context_data(self, **kwargs):
        if self.request.user.is_authenticated:
            print('User known')
        else:
            print('Anonymous')
