import os
from django.shortcuts import render
from django.conf import settings
from django.http import HttpResponse

from . import database
from .models import PageView


def index(request):
    hostname = os.getenv('HOSTNAME', 'unknown')
    PageView.objects.create(hostname=hostname)

    db_name = os.getenv('DATABASE_NAME')
    db_user = os.getenv('DATABASE_USER')
    engine = os.getenv('DATABASE_ENGINE')
    host = os.getenv('DATABASE_HOST')

    print('db name, user, engine, host')
    print(db_name)
    print(db_user)
    print(engine)
    print(host)

    return render(request, 'welcome/index.html', {
        'hostname': hostname,
        'database': database.info(),
        'count': PageView.objects.count(),
    })


def health(request):
    return HttpResponse(PageView.objects.count())
